
  // with jQuery

  $(document).ready(function(){
    $('#mobile-demo').sidenav({ edge: 'left' });
    $('#contact-me').sidenav({ edge: 'right' });
    $('.parallax').parallax();
    $('.sidenav').sideNav({
      draggable: true, // Choose whether you can drag to open on touch screens,
        });
    $('#textarea1').val('New Text');
    M.textareaAutoResize($('#textarea1'));
  });
  